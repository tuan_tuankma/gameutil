package com.game.util.filewatcher;


import java.util.ArrayList;
import java.util.List;

public class ListHandlerImpl<T> extends AbstractFileChangeHandler<List<T>> {
    private Class<T> clzz;

    public ListHandlerImpl(String file, Class<T> clzz) {
        super(file);
        this.clzz = clzz;
    }

    @Override
    protected List<T> parseData(String content) {
        List<T> data = new ArrayList<>();
        String[] splits = content.split("\n");
        for (int i = 0, size = splits.length; i < size; i++) {
            String s = splits[i].trim();
            T t = parseItem(s);
            if (t != null) {
                data.add(t);
            }
        }
        return data;
    }

    protected T parseItem(String s) {
        return ObjectUtil.parse(s, clzz);
    }
}
