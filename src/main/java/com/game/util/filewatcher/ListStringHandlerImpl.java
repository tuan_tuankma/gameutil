package com.game.util.filewatcher;


public class ListStringHandlerImpl extends ListHandlerImpl<String> {
    public ListStringHandlerImpl(String file) {
        super(file, String.class);
    }

    @Override
    protected String parseItem(String s) {
        if (!TextUtil.isEmpty(s)) {
            return s;
        }
        return null;
    }
}
