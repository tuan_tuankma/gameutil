package com.game.util.filewatcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class ObjectUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T parse(String json, Class<T> clazz) {
        if(json == null){
            return null;
        }
        if(clazz == String.class){
            return (T) json;
        }
        try {
            return objectMapper
                    .readerFor(clazz)
                    .readValue(json);
        } catch (IOException e) {
            LoggerFactory.getLogger(ObjectUtil.class).error("parse "+json+" clzz "+clazz,e);
        }
        return null;
    }

    public static <T> T parse(String json, TypeReference<T> clazz) {
        if(json == null){
            return null;
        }
        try {
            return objectMapper
                    .readValue(json,clazz);
        } catch (IOException e) {
            LoggerFactory.getLogger(ObjectUtil.class).error("parse "+json+" clzz "+clazz,e);
        }
        return null;
    }



    public static String toJsonString(Object obj) {
        try {
            return objectMapper
                    .writeValueAsString(obj);
        } catch (IOException e) {
            LoggerFactory.getLogger(ObjectUtil.class).error("toJsonString "+obj,e);
        }
        return null;
    }

    public static <T> T instantiate(Class<T> clazz) {
        try {
            T t = clazz.newInstance();
            return t;
        } catch (InstantiationException e) {

            LoggerFactory.getLogger(ObjectUtil.class).error("instantiate "+clazz,e);
        } catch (IllegalAccessException e) {
            LoggerFactory.getLogger(ObjectUtil.class).error("instantiate "+clazz,e);
        }
        return null;
    }

    public static <T> T instantiate(Class<T> clazz, Object... objects) {
        try {
            Class[] objs = new Class[objects.length];
            int size = objects.length;
            for (int i = 0; i < size; i++) {
                objs[i] = objects[i].getClass();
            }
            Constructor<?> cons = clazz.getConstructor(objs);
            T t = (T) cons.newInstance(objects);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            LoggerFactory.getLogger(ObjectUtil.class).error("instantiate "+clazz+" "+objects,e);
        }
        return null;

    }
}
