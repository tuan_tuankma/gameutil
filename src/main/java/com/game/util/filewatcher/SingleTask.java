package com.game.util.filewatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;

public abstract class SingleTask<T> implements Callable<T> {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    protected final List<Runnable> runOnLoopThread = new CopyOnWriteArrayList<>();
    public boolean finished = false;
    protected long currentTime;
    protected long timeElapsed;
    protected long startTIme;
    protected boolean shutDown;
    protected boolean started;
    protected boolean running;
    protected boolean error;
    protected boolean loopUpdate = true;
    protected boolean enableTimeOut;
    protected Future future;


    @Override
    public T call() throws Exception {
        running = true;
        timeElapsed = 0;
        currentTime = System.currentTimeMillis();
        startTIme = System.currentTimeMillis();
        try {
            start();
            onCall();
        } catch (Exception e) {
            LoggerFactory.getLogger(getClass()).error("error on task " + getClass(), e);
        } finally {
            stop();
        }

        return returnToPool();
    }

    protected void onCall() throws Exception {

    }

    protected void start() throws Exception {
        started = true;
        onStart();
    }
    protected void stop() throws Exception {
        finished = true;
        onStopped();
    }

    protected void onHandleErrorOnLoop(Exception e) {

    }

    protected long getIdleTime() {
        return 500;
    }

    void runOnLoopThread() {
        for (Runnable runnable : runOnLoopThread) {
            try {
                runnable.run();
            } catch (Exception e) {
                logger.error("Ex: ", e);
            }
        }
        runOnLoopThread.clear();
    }

    public void runOnLoopThread(Runnable runnable) {
        runOnLoopThread.add(runnable);
    }

    protected void onStart() {

    }

    protected abstract T returnToPool();

    protected void onStopped() throws Exception {
        running = false;
//        LoggerFactory.getLogger(getClass()).info("finish "+getClass());
    }

    private boolean interrupt() {
        return !finished;
    }

    public boolean isRunning() {
        return running && !finished;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    boolean internalUpdateTime() {
        return interrupt();
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isShutDown() {
        return shutDown;
    }

    public void setShutDown(boolean shutDown) {
        this.shutDown = shutDown;
    }

    public void timeout() {
//            Thread.interrupted();
        future.cancel(false);
    }
}
