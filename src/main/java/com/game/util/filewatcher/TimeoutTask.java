package com.game.util.filewatcher;

public class TimeoutTask extends Loop<TimeoutTask> {

    public TimeoutTask() {
        enableTimeOut = true;
    }


    @Override
    protected void start() throws Exception {
        super.start();
    }

    @Override
    protected void loopUpdate(long timeElapsed) {

    }

    @Override
    protected TimeoutTask returnToPool() {
        return this;
    }


}
