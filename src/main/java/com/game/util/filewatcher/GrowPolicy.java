package com.game.util.filewatcher;

import org.slf4j.LoggerFactory;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class GrowPolicy implements RejectedExecutionHandler {

    private final Lock lock = new ReentrantLock();
    private int increaseSize = 1;
    public GrowPolicy() {
    }

    public GrowPolicy(int increaseSize) {
        this.increaseSize = increaseSize;
    }

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        LoggerFactory.getLogger(getClass()).info("rejectedExecution");
        lock.lock();
        try {
            executor.setCorePoolSize(executor.getCorePoolSize() + increaseSize);
        } finally {
            lock.unlock();
        }

        executor.submit(r);
    }
}