package com.game.util.filewatcher;

public abstract class DelayWorker extends Loop<DelayWorker> {
    private long c;
    private long time;

    public DelayWorker(long time) {
        this.time = time;
    }

    @Override
    protected void loopUpdate(long timeElapsed) {
        c+= timeElapsed;
        if(c >= time){
           try{
               doWork();
           }catch (Exception e){
               e.printStackTrace();
           }
            c = 0;
        }
    }

    protected abstract void doWork();

    @Override
    protected DelayWorker returnToPool() {
        return this;
    }
}
