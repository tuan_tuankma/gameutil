package com.game.util.filewatcher;

public class TextUtil {


    public static boolean isEmpty(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    public static boolean isEmpty(String s) {
        if (s == null) {
            return true;
        }
        return isEmpty(s.subSequence(0, s.length()));
    }


    public static boolean equal(String s, String s1) {
        if (s == null || s1 == null) {
            return false;
        }
        return s.equals(s1);
    }

    public static boolean isEmpty(byte[] bS) {
        if (bS == null) {
            return true;
        }
        String s = new String(bS);
        return isEmpty(s.subSequence(0, s.length()));
    }
}
