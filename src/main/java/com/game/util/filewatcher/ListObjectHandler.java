package com.game.util.filewatcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class ListObjectHandler<T> extends AbstractFileChangeHandler<List<T>> {
    public ListObjectHandler(String file) {
        super(file);
    }

    @Override
    protected List<T> parseData(String content) {
        try {
            return new ObjectMapper().readValue(content, new TypeReference<List<T>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
