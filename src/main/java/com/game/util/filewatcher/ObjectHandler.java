package com.game.util.filewatcher;


public class ObjectHandler<T> extends AbstractFileChangeHandler<T> {
    private Class<T> clzz;

    public ObjectHandler(String file, Class<T> clzz) {
        super(file);
        this.clzz = clzz;
    }

    @Override
    protected T parseData(String content) {
        return ObjectUtil.parse(content, clzz);
    }
}
