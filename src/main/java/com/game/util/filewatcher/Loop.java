package com.game.util.filewatcher;

public abstract class Loop<T> extends SingleTask<T> {


    public Loop() {
        loopUpdate = true;
    }

    @Override
    protected void onCall() throws Exception {
        super.onCall();
        while (!Thread.currentThread().isInterrupted() && internalUpdateTime()) {
            long tmp = System.currentTimeMillis();
            timeElapsed = tmp - currentTime;
            currentTime = tmp;
            try {
                runOnLoopThread();
                loopUpdate(timeElapsed);
            } catch (Exception e) {
                if (!error) {
                    logger.error("Ex: ", e);
                }
                onHandleErrorOnLoop(e);
                error = true;
            }
            Thread.sleep(getIdleTime());
        }
    }

    protected abstract void loopUpdate(long timeElapsed);
}
