package com.game.util.filewatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public abstract class AbstractFileChangeHandler<T> extends AbstractFileChangeCallback {
    public Logger logger = LoggerFactory.getLogger(getClass());
    protected T data;
    protected String file;

    public AbstractFileChangeHandler(String file) {
        this.file = file;
    }

    public void load() {
        internalLoad();
    }

    protected void internalLoad(){
        try {
            String content = readFile(file);
            data = parseData(content);
            logger.info("loaded data  " + file);
            onDataChange(data);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("error  " + e);
        }
    }

    private String readFile(String file) throws IOException {
        FileInputStream fi = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            fi = new FileInputStream(file);
            isr = new InputStreamReader(fi, "utf-8");
            br = new BufferedReader(isr);
            String sR = "", sNewLine;

            while ((sNewLine = br.readLine()) != null) {
                byte[] bytes = sNewLine.getBytes(StandardCharsets.UTF_8);
                String utf8EncodedString = new String(bytes, StandardCharsets.UTF_8);
                sR += utf8EncodedString + "\n";

            }
            return sR;
        } catch (Exception e) {

        } finally {
            if (fi != null) {
                fi.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (br != null) {
                br.close();
            }
        }

        return null;
    }

    @Override
    public void onFileChange(String file) {
        load();
    }

    public T getData() {
        if (data == null) {
            load();
        }
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    protected abstract T parseData(String content);

    protected void onDataChange(T data) {

    }
}
