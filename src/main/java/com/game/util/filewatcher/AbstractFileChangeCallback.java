package com.game.util.filewatcher;

public class AbstractFileChangeCallback implements FileChangeCallback {
    @Override
    public void onFileCreated(String file) {

    }

    @Override
    public void onFileDeleted(String file) {

    }

    @Override
    public void onFileChange(String file) {

    }

    @Override
    public void onFolderChange(String folder) {

    }
}
