package com.game.util.filewatcher;

public class LoopManagerAdapter {

    public LoopManager createInstance(Class<? extends LoopManager> clzz) {
        return ObjectUtil.instantiate(clzz);
    }
}
