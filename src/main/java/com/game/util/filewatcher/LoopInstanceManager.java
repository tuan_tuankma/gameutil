package com.game.util.filewatcher;

import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class LoopInstanceManager {
    private static volatile  LoopInstanceManager instance;
    private Map<Class<? extends LoopManager>, LoopManager> instances = new HashMap<>();
    private LoopManagerAdapter adapter = new LoopManagerAdapter();
    static {
        instance = new LoopInstanceManager();
    }

    public static LoopInstanceManager getInstance() {
        if (instance == null) {
            instance = new LoopInstanceManager();
        }
        return instance;
    }

    public void setInstanceAdapter(LoopManagerAdapter adapter) {
        if (adapter == null) {
            return;
        }
        this.adapter = adapter;
    }

    public LoopManager getLoopManager(Class<? extends LoopManager> clzz) {
        LoopManager manager = null;
        synchronized (instances){
            manager = instances.get(clzz);
            if (manager == null) {
                manager = adapter.createInstance(clzz);
                LoggerFactory.getLogger(getClass()).info("create "+clzz);
                instances.put(clzz, manager);
            }

        }

        return manager;
    }
    public static void main(String[] args) {
        CountDownLatch lat = new CountDownLatch(1000);
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("start "+ finalI+Thread.currentThread());
                    LoopInstanceManager.getInstance().getLoopManager(new Random().nextBoolean() ?LoopManager.class:MTest.class);
                    System.out.println("fnish "+ finalI +Thread.currentThread());
                    lat.countDown();;
                }
            }).start();
        }
        try {
            lat.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 1000; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("start "+ finalI+Thread.currentThread());
                    LoopInstanceManager.getInstance().getLoopManager(new Random().nextBoolean() ?LoopManager.class:MTest.class);
                    System.out.println("fnish "+ finalI +Thread.currentThread());
                }
            }).start();
        }
    }

    public static final class MTest extends LoopManager{

    }
}
