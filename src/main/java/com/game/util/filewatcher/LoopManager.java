package com.game.util.filewatcher;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LoopManager {
    private static AtomicInteger count = new AtomicInteger();
    private final ScheduledThreadPoolExecutor executorService;
    private final TimoutManager timoutManager;
    private final Lock lock = new ReentrantLock();
    private int maxUnUseThread = 200;
    private int maxThread;

    public LoopManager() {
        this(10, 300000,1000);
    }

    public LoopManager(Integer poolSize) {
        this(poolSize, 300000,1000);
    }

    public LoopManager(Integer poolSize, int timeoutInterval,int maxThread) {
        executorService = new ExtendedScheduledThreadPoolExecutor(maxThread,getClass(),poolSize, new GameThreadFactory(getClass().getSimpleName()), new GrowPolicy(10));
        timoutManager = new TimoutManager(timeoutInterval, this);
        executorService.submit(timoutManager);
    }

    public ScheduledThreadPoolExecutor getExecutorService() {
        return executorService;
    }

    public Future addLooper(Callable loop) {
        if (loop instanceof TimeoutTask) {
            return addTimeoutLooper((TimeoutTask) loop);
        } else {
            return executorService.submit(loop);
        }
    }

    public Future addTimeoutLooper(TimeoutTask loop) {
        Future future = executorService.submit(loop);
        loop.future = future;
        timoutManager.add(loop);
        return future;
    }

    public Future<?> addTask(Callable loop) {
        return addLooper(loop);
    }

    public Future<?> addTask(Runnable loop) {
        if (loop instanceof TimeoutTask) {
            return addTimeoutLooper((TimeoutTask) loop);
        } else {
            return executorService.submit(loop);
        }
    }

    public void setMaxUnUseThread(int maxUnUseThread) {
        this.maxUnUseThread = maxUnUseThread;
    }

    public static void main(String[] args) {
        Configurator.setRootLevel(Level.DEBUG);
        LoopManager manager = new LoopManager();

        for (int i = 0; i < 1000; i++) {
            manager.addLooper(new TimeoutTask() {
                @Override
                protected void onStart() {
                    super.onStart();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 1000; i++) {
            manager.addLooper(new TimeoutTask() {
                @Override
                protected void loopUpdate(long timeElapsed) {
                    super.loopUpdate(timeElapsed);
//                    finished = true;
//                    LoggerFactory.getLogger(getClass()).info("loopUpdate1 "+getClass());
                }
            });
        }
    }

    private static final class TimoutManager extends DelayWorker {
        List<SingleTask> futures = new ArrayList<>();
        long timeout;
        private LoopManager loopManager;

        public TimoutManager(long timeout, LoopManager loopManager) {
            super(10000);
            this.timeout = timeout;
            this.loopManager = loopManager;
        }

        private void add(SingleTask timeoutTask) {
            synchronized (futures) {
                futures.add(timeoutTask);
            }
        }

        @Override
        protected void doWork() {
            synchronized (futures) {
////                LoggerFactory.getLogger(getClass()).info("active " + loopManager.executorService.getActiveCount() + " queue " + loopManager.executorService.getQueue().size() + " future " + futures.size());
//                LoggerFactory.getLogger(getClass()).info(
//                        String.format("[monitor] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s",
//                                loopManager.executorService.getPoolSize(),
//                                loopManager.executorService.getCorePoolSize(),
//                                loopManager.executorService.getActiveCount(),
//                                loopManager.executorService.getCompletedTaskCount(),
//                                loopManager.executorService.getTaskCount(),
//                                loopManager.executorService.isShutdown(),
//                                loopManager.executorService.isTerminated()));

                for (int i = 0; i < futures.size(); i++) {
                    SingleTask future = futures.get(i);
                    if ( System.currentTimeMillis() - future.startTIme >= timeout) {
                        future.finished = true;
                        future.timeout();
                        LoggerFactory.getLogger(getClass()).info("task " + future + " timeout");
                    }
                }
                for (int i = 0; i < futures.size(); i++) {
                    SingleTask future = futures.get(i);
                    if (future.finished) {
                        future.finished = true;
                        futures.remove(i);
                        i--;
                    }
                }
                Map<String, Long> counts = new HashMap<>();
                for (int i = 0; i < futures.size(); i++) {
                    SingleTask future = futures.get(i);
                    Long tmp = counts.get(future.getClass().getSimpleName());
                    if (tmp == null) {
                        tmp = new Long(0);
                        counts.put(future.getClass().getSimpleName(), tmp);
                    } else {
                        tmp += 1;
                        counts.put(future.getClass().getSimpleName(), tmp);
                    }
                }
                for (String key : counts.keySet())
                    LoggerFactory.getLogger(getClass()).info("key " + key + " count " + counts.get(key));

            }
        }
    }

}
