package com.game.util.filewatcher;

import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ExtendedScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {
    private final Lock lock = new ReentrantLock();
    private int maxUnUseThread = 100;
    private int maxThread;
    private Class<? extends LoopManager> aClass;

    public ExtendedScheduledThreadPoolExecutor(Class<? extends LoopManager> aClass, int corePoolSize) {
        super(corePoolSize);
        this.aClass = aClass;
    }

    public ExtendedScheduledThreadPoolExecutor(Class<? extends LoopManager> aClass, int corePoolSize, ThreadFactory threadFactory) {
        super(corePoolSize, threadFactory);
        this.aClass = aClass;
    }

    public ExtendedScheduledThreadPoolExecutor(Class<? extends LoopManager> aClass, int corePoolSize, RejectedExecutionHandler handler) {
        super(corePoolSize, handler);
        this.aClass = aClass;
    }

    public ExtendedScheduledThreadPoolExecutor(int maxThread,Class<? extends LoopManager> aClass, int corePoolSize, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, threadFactory, handler);
        this.maxThread = maxThread;
        this.aClass = aClass;
    }

    @Override
    public void execute(Runnable command) {
        validateThread(command);
        super.execute(command);
    }

    @Override
    public Future<?> submit(Runnable task) {
        validateThread(task);
        return super.submit(task);
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        validateThread(task);
        return super.submit(task, result);
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        validateThread(task);
        return super.submit(task);
    }

    private void validateThread(Object object) {
//        lock.tryLock();
//        try {
        checkActiveThread(object);
//        } finally {
//            lock.unlock();
//        }
    }

    private void checkActiveThread(Object o) {
//        LoggerFactory.getLogger(getClass()).info(
//                        String.format("[monitor] [%d/%d] Active: %d, Completed: %d, Task: %d, isShutdown: %s, isTerminated: %s",
//                                getPoolSize(),
//                                getCorePoolSize(),
//                                getActiveCount(),
//                                getCompletedTaskCount(),
//                                getTaskCount(),
//                               isShutdown(),
//                                isTerminated()));
        if (getActiveCount() >= getCorePoolSize() && getCorePoolSize() < maxThread) {
            setCorePoolSize(getCorePoolSize() + 1);
        }
    }

    private void releaseThread() {
        if (getCorePoolSize() - getActiveCount() >= maxUnUseThread) {
            setCorePoolSize(getCorePoolSize() - maxUnUseThread);
        }
    }

    public void setMaxUnUseThread(int maxUnUseThread) {
        this.maxUnUseThread = maxUnUseThread;
    }
}
