package com.game.util.filewatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FolderWatcher extends Loop<FolderWatcher> {
    private static Logger log = LoggerFactory.getLogger(FolderWatcher.class);
    protected boolean debug = false;
    protected String folder;
    private WatchService watchService;
    private Map<String, FileChangeCallback> callbacks = new HashMap<>();

    public FolderWatcher() {
    }

    public FolderWatcher(String folder) {
        this(true, folder);
    }

    public FolderWatcher(boolean debug, String folder) {
        this.debug = debug;
        this.folder = folder;
    }

    public void registerCallback(String file, FileChangeCallback callback) {
        callbacks.put(file, callback);
    }

    public void unregisterCallback(String file) {
        callbacks.remove(file);
    }

    @Override
    protected void onStart() {
        super.onStart();
        watchService
                = null;
        try {
            watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(folder);

            path.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void loopUpdate(long timeElapsed) {
        try {
            WatchKey key;
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    if (event.kind() == (StandardWatchEventKinds.ENTRY_MODIFY))
                        notifyEventFileChange(String.valueOf(event.context()));
                    else if (event.kind() == (StandardWatchEventKinds.ENTRY_CREATE))
                        notifyEventFileCreated(String.valueOf(event.context()));
                    else if (event.kind() == (StandardWatchEventKinds.ENTRY_DELETE))
                        notifyEventFileDeleted(String.valueOf(event.context()));
                }
                key.reset();
                notifyEventFolderChange(folder);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void notifyEventFileChange(String context) {
        Set<Map.Entry<String, FileChangeCallback>> entrySet = callbacks.entrySet();
        Iterator<Map.Entry<String, FileChangeCallback>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, FileChangeCallback> entry = iterator.next();
            if (entry.getKey().equals(folder + context)) {
                entry.getValue().onFileChange(folder + context);
            }
        }
        if (debug) {
            log.info("file change " + context);
        }
    }

    private void notifyEventFileDeleted(String context) {
        Set<Map.Entry<String, FileChangeCallback>> entrySet = callbacks.entrySet();
        Iterator<Map.Entry<String, FileChangeCallback>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, FileChangeCallback> entry = iterator.next();
            if (entry.getKey().equals(context)) {
                entry.getValue().onFileDeleted(context);
            }
        }
        if (debug) {
            log.info("file deleted " + context);
        }
    }

    private void notifyEventFileCreated(String context) {
        Set<Map.Entry<String, FileChangeCallback>> entrySet = callbacks.entrySet();
        Iterator<Map.Entry<String, FileChangeCallback>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, FileChangeCallback> entry = iterator.next();
            if (entry.getKey().equals(context)) {
                entry.getValue().onFileCreated(context);
            }
        }
        if (debug) {
            log.info("file created " + context);
        }
    }

    private void notifyEventFolderChange(String folder) {
        Set<Map.Entry<String, FileChangeCallback>> entrySet = callbacks.entrySet();
        Iterator<Map.Entry<String, FileChangeCallback>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, FileChangeCallback> entry = iterator.next();
            entry.getValue().onFolderChange(folder);
        }
        if (debug) {
            System.out.println("folder change " + folder);
        }
    }

    @Override
    protected FolderWatcher returnToPool() {
        return this;
    }

}
