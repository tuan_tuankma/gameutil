package com.game.util.filewatcher;

public interface FileChangeCallback {
    void onFileCreated(String file);

    void onFileDeleted(String file);

    void onFileChange(String file);

    void onFolderChange(String folder);
}
