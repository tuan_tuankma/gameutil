package com.game.util.filewatcher;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class ObjectUtilTest {

    @org.junit.Test
    public void parse() {
        String jsonInput = "{\"test\": {\"c\":\"1\",\"k\":{\"c\":\"10\"}},\"test2\": {\"c\":\"2\"}}";
        Map<String, Test> map = ObjectUtil.parse(jsonInput,new TypeReference<HashMap<String, Test>>() {});
        System.out.println(map.get("test").k.c);
        assert(map.get("test").k.c.equals("10"));
        assert(map.get("test").c.equals("1"));
        assert(map.get("test2").c.equals("2"));
    }

    public static final class Test{
        private String c;
        private Test2 k;

        public Test2 getK() {
            return k;
        }

        public void setK(Test2 k) {
            this.k = k;
        }

        public String getC() {
            return c;
        }

        public void setC(String c) {
            this.c = c;
        }
    }

    private static final class Test2{
        private String c;

        public String getC() {
            return c;
        }

        public void setC(String c) {
            this.c = c;
        }
    }
}